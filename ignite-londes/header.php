<!DOCTYPE html>
<head>

    <!--[if lt IE 7 ]> <html class="ie6" <?php language_attributes(); ?>> <![endif]-->
    <!--[if IE 7 ]>    <html class="ie7" <?php language_attributes(); ?>> <![endif]-->
    <!--[if IE 8 ]>    <html class="ie8" <?php language_attributes(); ?>> <![endif]-->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv/html5shiv.js"></script>
    <![endif]-->
    <!--[if IE 9 ]><html class="ie9" <?php language_attributes(); ?>> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title("",true); ?></title>

    <!-- set width to the device viewing the site -->
    <meta name="viewport" content="width=device-width" />

    <?php wp_head(); ?>
	<link href='http://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58694764-1', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body id="<?php print get_stylesheet(); ?>" <?php body_class(); ?>>

<!--skip to content link-->
<a class="skip-content" href="#main">Skip to content</a>

<div>


<header class="site-header" id="site-header" role="banner">
	
	<div>
		<center><a href="<?php echo esc_url(home_url()); ?>"><img src="/wp-content/uploads/2014/01/logo.gif" width="120" alt="Albion Alumni Foundation"><br/><h1 style="font-size:26px;">Albion High School Alumni Foundation, Inc.</h1></a>
		<span style="font-family: 'Alex Brush', cursive; font-size: 28px;">Making a Difference For a Lifetime</span></center></div>
	
	<?php get_template_part( 'menu', 'primary' ); // adds the primary menu ?>

</header>
<div id="overflow-container" class="overflow-container">
<!--
<div class="breadcrumbs" style="margin-top:-20px;">
<?php
if ( is_front_page() ) {
	if ( function_exists( 'soliloquy' ) ) { soliloquy( '289' ); } 
	if ( function_exists( 'soliloquy' ) ) { soliloquy( 'homepage-slider', 'slug' ); }
}
?>
</div>
    <?php
    if ( current_theme_supports( 'breadcrumb-trail' ) && !is_search() ) {
        breadcrumb_trail(array(
            'separator' => '>',
            'show_browse' => false,
            'show_on_front' => false)
        );
    }
    ?>
-->
    <div id="main" class="main" role="main">
    
   