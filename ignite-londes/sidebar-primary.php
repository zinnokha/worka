<?php if ( is_active_sidebar( 'primary' ) ) : ?>

    <div class="sidebar sidebar-primary" id="sidebar-primary" role="complementary">
			    

		<?php
			if ( ! is_user_logged_in() )
			echo '<!-- <p><a href="/membership-donations/">Donate</a></p> -->';
			?>

        <?php dynamic_sidebar( 'primary' ); ?>
<div style="text-align:center; margin-left:20px;">
		<p class="text-center"><a href="https://www.facebook.com/groups/51862803147/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.jpg" style="width:140px; float:left; display:inline;" alt="Albion Alumni Facebook"></a>
<a href="https://instagram.com/AlbionHSAlumni/" target="_blank"><img src="http://albionalumni.org/wp-content/uploads/2015/06/instagram-icon.png" style="height:50px; float:left; display:inline;" alt="Albion Alumni Instagram"></a>
<a href="https://twitter.com/AlbionHSAlumni" target="_blank"><img src="http://albionalumni.org/wp-content/uploads/2015/06/twitter-icon.png" style="height:50px; float:left; display:inline;" alt="Albion Alumni Twitter"></a>
		<?php //ct_ignite_social_media_icons(); // adds social media icons ?>
</div>
    </div><!-- #sidebar-primary -->

<?php endif; ?>