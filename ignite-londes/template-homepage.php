<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>



<?php

// The loop
if ( have_posts() ) :
    while (have_posts() ) : 
        the_post();
        get_template_part( 'content-page' ); 
        //comments_template();
    endwhile;
endif;
?>

<?php
// The Query
query_posts( 'cat=-6&posts_per_page=4' );

// The loop
if ( have_posts() ) :
    while (have_posts() ) : 
        the_post();
        get_template_part('content');
    endwhile;
endif; 
// Reset Query
wp_reset_query();
?>



<?php get_footer(); ?>