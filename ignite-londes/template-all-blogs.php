<?php
/**
 * Template Name: All Blogs
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>



<?php
// The Query
/*
if ( is_user_logged_in() )
	query_posts( '&posts_per_page=4' );
else
	query_posts( 'cat=-6&posts_per_page=4' );
*/

		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

query_posts( 'posts_per_page=10&paged=' .$paged );

// The loop
if ( have_posts() ) :
    while (have_posts() ) : 
        the_post();
        get_template_part('content');
    endwhile;
endif; 

 ct_ignite_post_navigation(); 


// Reset Query
wp_reset_query();
?>



<?php get_footer(); ?>