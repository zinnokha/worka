</div> <!-- .main -->

<div id="sidebar-primary-container" class="sidebar-primary-container">
    <?php get_sidebar( 'primary' ); ?>
</div>

</div> <!-- .overflow-container -->
<div id="pre-footer">
	<div class="section-container">
<?php
//echo do_shortcode('[contact-form-7 id="331" title="Footer Form"]');
?>
	<div class="clear"></div>
	</div>
	
</div>
<footer class="site-footer" role="contentinfo">

<div class="footer-nav">
	<div class="section">	<?php wp_nav_menu( array( 'theme_location' => 'footer1', 'depth' => 1 ) ); ?></div>
	<div class="section">	<?php wp_nav_menu( array( 'theme_location' => 'footer2', 'depth' => 1 ) ); ?></div>
	<div class="section">	<?php wp_nav_menu( array( 'theme_location' => 'footer3', 'depth' => 1 ) ); ?></div>
	<div class="section">	<?php wp_nav_menu( array( 'theme_location' => 'footer4', 'depth' => 1 ) ); ?></div>
	<div class="section contact-form" ></div>
</div>
<div class="clear"></div>

    <div class="design-credit">
    <h3><a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('title'); ?></a> - <a href="http://www.londes.com" title="LDM Inc"><img src="http://www.londes.com/wp-content/uploads/2013/11/ldm-web-design.png" alt="LDM Digital Marketing Services" rel="nofollow"></a></h3> 
    <span><?php bloginfo('description'); ?></span>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>